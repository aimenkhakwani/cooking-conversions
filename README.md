# A Simple Cooking App

#### _This is a simple javascript conversion tool for cookers! It will alow you to convert between different units. 8/8/16._

#### By _**Aimen Khakwani and Kyle Lange**_

## Description

_This javascript cooking app will convert between different cooking units._

## Setup/Installation Requirements

*_clone from github onto your local directory

## Known Bugs

_None so far..._

## Support and Contact Details

_Please contact us through github_

## Technologies Used

_html5, CSS 3 with bootstrap, and javascript_

### License

Copyright (c) 2016 _**Aimen Khakwani and Kyle Lange**_
